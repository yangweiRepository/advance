package com.carbon.dal.datasource;



import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AdvanceShardingDataSource  extends AbstractRoutingDataSource {

    private final Map<Boolean, DataSource> dataSourceMap;

    public AdvanceShardingDataSource(DataSource advanceWriteAndReadDataSource, DataSource shardingDataSource) {

        HashMap<Boolean, DataSource> targetDataSourceMap = Maps.newHashMap();
        targetDataSourceMap.put(Boolean.FALSE, advanceWriteAndReadDataSource);
        targetDataSourceMap.put(Boolean.TRUE, shardingDataSource);
        this.dataSourceMap = targetDataSourceMap;
    }

    @Override
    protected Object determineCurrentLookupKey() {
        System.out.println("============分片数据源========");
        return DataSourceContextHolder.getJustOnce();
    }
    @Override
    public void afterPropertiesSet() {
        this.setDefaultTargetDataSource(dataSourceMap.get(Boolean.TRUE));
        this.setTargetDataSources((Map) this.dataSourceMap);
        super.afterPropertiesSet();
    }
}
