package com.carbon.dal.datasource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;


import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 读写数据源
 */
@Slf4j
public class AdvanceWriteAndReadDataSource extends AbstractRoutingDataSource {

    private final String dataSourceName="ADVANCE";
    private final Map<ReadModel, DataSource> dataSourceMap;


    /**
     * ThreadLocal 用于提供线程局部变量，在多线程环境可以保证各个线程里的变量独立于其它线程里的变量。
     * 也就是说 ThreadLocal 可以为每个线程创建一个【单独的变量副本】，相当于线程的 private static 类型变量。
     */
    private static final ThreadLocal<ReadModel> CONTEXT_HOLDER = new ThreadLocal<>();


//    public AdvanceWriteAndReadDataSource(String dataSourceName, Map<ReadModel, DataSource> targetDataSources) {
//        this.dataSourceName = dataSourceName;
//        this.dataSourceMap = targetDataSources;
//    }

    public AdvanceWriteAndReadDataSource(DataSource masterDataSource,  Map<ReadModel, DataSource> targetDataSources) {
        this.dataSourceMap = targetDataSources;
    }

    /**
     * 获取读写数据源
     */
//    public static AdvanceWriteAndReadDataSource  genAdvanceWriteAndReadDataSource(DataSource masterDataSource,  DataSource slaveDataSource) {
//        Map<ReadModel, DataSource> targetDataSources = new HashMap<>();
//        String dataSourceName = "ADVANCE";
//        targetDataSources.put(ReadModel.MASTER, masterDataSource);
//        targetDataSources.put(ReadModel.SLAVE, slaveDataSource);
//        return new AdvanceWriteAndReadDataSource(dataSourceName, targetDataSources);
//    }


    /**
     * determineCurrentLookupKey决定了使用哪个数据源
     * @return
     */
    @Override
    protected Object determineCurrentLookupKey() {
        if ("ADVANCE".equals(dataSourceName)) {
            return getDataSource();
        }
        throw new RuntimeException(dataSourceName + ", can not process");
    }

    @Override
    public void afterPropertiesSet() {
        this.setDefaultTargetDataSource(dataSourceMap.get(ReadModel.MASTER));
        this.setTargetDataSources((Map) dataSourceMap);
        log.info("Init data source({})", dataSourceName);
        super.afterPropertiesSet();
    }

    /**
     * 如果说切面那里的@CurrentDataSource注解为null就取初始化的默认值
     * @return
     */
    private Object getDataSource() {
        /**
         * 这里的打印结果说明了，如果没有@CurrentDataSource注解
         * CONTEXT_HOLDER.get()为空，那么在AbstractRoutingDataSource的determineTargetDataSource中会去拿
         * 默认的datasource
         */
//        log.debug("数据源为："+CONTEXT_HOLDER.get() );
//        System.out.println("数据源为：" + CONTEXT_HOLDER.get());
        if (null != CONTEXT_HOLDER.get()) {
            System.out.println("========="+CONTEXT_HOLDER.get().name());
        }

        return CONTEXT_HOLDER.get();
    }

    /**
     * 切面的时候设置datasource
     * @param readModel
     */
    public static void setDataSource( ReadModel readModel) {
         CONTEXT_HOLDER.set(readModel);
    }

    /**
     * 每次数据源使用完毕后，清除当前线程使用的数据源
     * @param
     */
    public static void clearDataSource() {
        CONTEXT_HOLDER.remove();
    }

}
