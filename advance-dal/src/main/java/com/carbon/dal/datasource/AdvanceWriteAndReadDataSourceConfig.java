package com.carbon.dal.datasource;


import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.carbon.dal.datasource.sharding.tableconfig.EnableTableConfig;
import com.google.common.collect.Maps;
import io.shardingsphere.api.config.rule.ShardingRuleConfiguration;
import io.shardingsphere.api.config.rule.TableRuleConfiguration;
import io.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 读写数据源配置类
 */
@Configuration
public class AdvanceWriteAndReadDataSourceConfig {


    /**
     * 根据读写数据源  生成 分表数据源
     * @param masterDataSource
     * @param slaveDataSource
     * @return
     */

//    @Bean("advanceShardingDataSource")
//    @Primary
//    public DataSource advanceShardingDataSource(@Qualifier("masterDataSource") DataSource masterDataSource,@Qualifier("slaveDataSource")DataSource slaveDataSource
//    ,@Autowired ConfigurableApplicationContext applicationContext) throws SQLException {
//        //原生读写数据源
//        AdvanceWriteAndReadDataSource advanceWriteAndReadDataSource = AdvanceWriteAndReadDataSource.genAdvanceWriteAndReadDataSource(masterDataSource, slaveDataSource);
//        advanceWriteAndReadDataSource.afterPropertiesSet();
//        /**
//         * 可以单独抽出来
//         */
//        // sharding数据源
//        // final Map<String, DataSource> dataSourceMap, final ShardingRuleConfiguration shardingRuleConfig, final Map<String, Object> configMap, final Properties props
//        Map<String, DataSource> dataSourceMap = Maps.newHashMap();
//        dataSourceMap.put("ds", advanceWriteAndReadDataSource);
//
//        //尚未实现
//        ShardingRuleConfiguration shardingRuleConfiguration = new ShardingRuleConfiguration();
//
//        shardingRuleConfiguration.getTableRuleConfigs().addAll(getAllRuleConfig(applicationContext));
//        DataSource shardingDataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfiguration, Maps.newHashMap(), new Properties());
//        // 动态路由需要分表的数据源
//        return shardingDataSource;
////        return new AdvanceShardingDataSource(advanceWriteAndReadDataSource,shardingDataSource);
//    }

//    private List<TableRuleConfiguration> getAllRuleConfig(ConfigurableApplicationContext applicationContext) {
//        String[] names = applicationContext.getBeanNamesForType(EnableTableConfig.class);
//        List<TableRuleConfiguration> configurations = new ArrayList<>(names.length);
//
//        for (String name : names) {
//            EnableTableConfig config = applicationContext.getBean(name, EnableTableConfig.class);
//            configurations.add(config.getConfig());
//        }
//
//        return configurations;
//    }

//    @Primary
//    @Bean("sqlSessionFactory")
//    public SqlSessionFactory sqlSessionFactory(@Qualifier("advanceShardingDataSource") DataSource dataSource) throws Exception {
//        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
//        sessionFactory.setDataSource(dataSource);
//        sessionFactory.setConfigLocation(new ClassPathResource("advance-mybatis-config.xml"));
//        return sessionFactory.getObject();
//    }

//    @Primary
//    @Bean("advanceMybatisSqlSessionFactory")
//    public SqlSessionFactory sqlSessionFactory(@Autowired DataSource dataSource) throws Exception {
//        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
//        sessionFactory.setDataSource(dataSource);
//        sessionFactory.setConfigLocation(new ClassPathResource("advance-mybatis-config.xml"));
//        return sessionFactory.getObject();
//    }


    /**
     * 读写数据库 AdvanceWriteAndReadDataSource配置
     * @param masterDataSource
     * @param slaveDataSource
     * @return
     */

    @Bean("advanceWriteAndReadDataSource")
    @Primary
    public DataSource AdvanceWriteAndReadDataSource(@Qualifier("masterDataSource") DataSource masterDataSource,@Qualifier("slaveDataSource") DataSource slaveDataSource) {
        Map<ReadModel, DataSource> targetDataSources = new HashMap<>();
        targetDataSources.put(ReadModel.MASTER, masterDataSource);
        targetDataSources.put(ReadModel.SLAVE, slaveDataSource);
        return new AdvanceWriteAndReadDataSource(masterDataSource, targetDataSources);
    }



    /**
     * 主库数据源
     * @return
     */
    @Bean("masterDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.druid.master")
    public DataSource getMasterDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 从库数据源
     * @return
     */
    @Bean("slaveDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.druid.slave")
    public DataSource getSlaveDataSource() {
        return DruidDataSourceBuilder.create().build();
    }
}
