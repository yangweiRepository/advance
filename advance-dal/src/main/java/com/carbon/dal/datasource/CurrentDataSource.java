package com.carbon.dal.datasource;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 该注解（给定的值）可以切换数据源
 * 使用注解的时候默认给定从库
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentDataSource {
    ReadModel name() default ReadModel.SLAVE;
}
