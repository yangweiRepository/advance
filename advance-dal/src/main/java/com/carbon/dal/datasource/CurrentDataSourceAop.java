package com.carbon.dal.datasource;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 配置CurrentDataSource注解的切面
 * 疑问？不使用CurrentDataSource的时候切面是否生效
 * order接口影响类的执行顺序，而不是加载顺序
 */
@Aspect
@Component
@Slf4j
@Order(1)
public class CurrentDataSourceAop {

    @Pointcut("@annotation(com.carbon.dal.datasource.CurrentDataSource)")
    public void currentDataSourcePointCut() {
        // pointcut只需要一个方法就够了
    }

    @Around("currentDataSourcePointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature =(MethodSignature)  point.getSignature();
        Method method = signature.getMethod();
        CurrentDataSource annotation = method.getAnnotation(CurrentDataSource.class);
        if (null == annotation) {
            /**
             * 测试下不加该注解的是否会触发切面
             * 不会触发，那么是不是证明这个代码有问题，判空不会执行
             */
            log.debug("CurrentDataSource is null");
            AdvanceWriteAndReadDataSource.setDataSource(ReadModel.MASTER);
        } else {
            AdvanceWriteAndReadDataSource.setDataSource(annotation.name());
        }
        try {
            return point.proceed();
        }finally {
            AdvanceWriteAndReadDataSource.clearDataSource();
        }
    }
}
