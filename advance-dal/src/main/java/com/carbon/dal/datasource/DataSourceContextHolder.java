package com.carbon.dal.datasource;

public class DataSourceContextHolder {
    /**
     * 是否使用分表数据源
     */
    private static final ThreadLocal<Boolean> REQUEST_SPLIT_DATA_SOURCE_HOLDER = ThreadLocal.withInitial(() -> true);

    public static void setIsSplitDataSource(boolean flag) {
        REQUEST_SPLIT_DATA_SOURCE_HOLDER.set(flag);
    }

    public static boolean getJustOnce() {
        return REQUEST_SPLIT_DATA_SOURCE_HOLDER.get();
    }

    public static void removeIsSpilt() {
        REQUEST_SPLIT_DATA_SOURCE_HOLDER.remove();
    }

}
