package com.carbon.dal.datasource.sharding.tableconfig;

import io.shardingsphere.api.config.rule.TableRuleConfiguration;

public interface EnableTableConfig {
    TableRuleConfiguration getConfig();
}
