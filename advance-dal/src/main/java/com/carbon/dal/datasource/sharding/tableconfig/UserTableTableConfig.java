package com.carbon.dal.datasource.sharding.tableconfig;

import com.carbon.dal.datasource.sharding.algorithm.UserAlgorithm;
import io.shardingsphere.api.config.rule.TableRuleConfiguration;
import io.shardingsphere.api.config.strategy.StandardShardingStrategyConfiguration;
import org.springframework.stereotype.Component;

@Component
public class UserTableTableConfig implements EnableTableConfig {

    @Override
    public TableRuleConfiguration getConfig() {

        TableRuleConfiguration tableRuleConfiguration = new TableRuleConfiguration();
        tableRuleConfiguration.setLogicTable("admin_user");
        tableRuleConfiguration.setActualDataNodes("ds.admin_user$->{0..2}");
        tableRuleConfiguration.setDatabaseShardingStrategyConfig(new StandardShardingStrategyConfiguration("id", new UserAlgorithm()));
        return tableRuleConfiguration;
    }

}
