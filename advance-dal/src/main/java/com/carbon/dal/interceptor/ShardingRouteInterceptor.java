package com.carbon.dal.interceptor;

import com.carbon.dal.datasource.DataSourceContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.util.Properties;
/**

 * @Description 设置请求是否需要使用sharding分表数据源

 */
@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class}),
        @Signature(type = Executor.class, method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
        @Signature(type = Executor.class, method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}),
})
@Slf4j
public class ShardingRouteInterceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement ms = (MappedStatement) invocation.getArgs()[0];
        // mapperId 是mybatis定义 的一个mapper方法的id, 值为method的全类名
        // 示例: com.mi.info.xms.masp.dal.mapper.masp.SettlementFeeDetailsMapper.insert

        String mapperId = ms.getId();
        System.out.println("==============="+mapperId);
        int pos = mapperId.lastIndexOf(".");
        String clsName = mapperId.substring(0, pos);
        Class cls = Class.forName(clsName);
        ShardingRouteMapper splitMapper = (ShardingRouteMapper) cls.getDeclaredAnnotation(ShardingRouteMapper.class);
        try {
            if (splitMapper != null) {
                DataSourceContextHolder.setIsSplitDataSource(true);
            }else {
                DataSourceContextHolder.setIsSplitDataSource(false);
            }
            return invocation.proceed();
        }finally {
            DataSourceContextHolder.removeIsSpilt();
        }
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
