package com.carbon.dal.mapper.user;

import com.carbon.dal.bean.User;
import com.carbon.dal.interceptor.ShardingRouteMapper;
import com.carbon.dal.query.UserQuery;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ShardingRouteMapper
public interface UserMapper {

    List<User> listByQuery(UserQuery query);

   User listById(@Param("id") Integer id);

    List<User> getUserAll();


    void insert(User user);

    Integer insert1(@Param("id") Integer id, @Param("name") String name);

    List<User> listAll();

    User getById(@Param("id") Integer id);

    User getByName(@Param("principal") String principal);
}
