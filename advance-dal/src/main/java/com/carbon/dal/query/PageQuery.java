package com.carbon.dal.query;

import lombok.Data;

@Data
public class PageQuery {
    private int pageNum;
    private int pageSize;
}
