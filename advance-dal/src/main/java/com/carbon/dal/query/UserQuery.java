package com.carbon.dal.query;

import lombok.Data;

@Data
public class UserQuery extends PageQuery{
    private Integer id;
    private String name;
}
