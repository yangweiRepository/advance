package com.carbon.dal.reponse;

import lombok.Data;

@Data
public class CommonResponse<T> {
    private Integer code;
    private String message;
    private T data;

    private CommonResponse() {
        this.code = 200;
        this.message = "success";
    }

    private CommonResponse(T data) {
        this.code = 200;
        this.message = "success";
        this.data = data;
    }

    private CommonResponse(CodeMsg codeMsg) {
        this.code = codeMsg.getCode();
        this.message = codeMsg.getMsg();
    }

    public static <T> CommonResponse<T> success(T data) {

        return new CommonResponse<>(data);
    }

    public static <T> CommonResponse<T> success() {

        return new CommonResponse<>();
    }

    public static <T> CommonResponse<T> error(CodeMsg codeMsg) {
        return new CommonResponse<>(codeMsg);
    }
}
