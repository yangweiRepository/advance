package com.carbon.config;


import com.carbon.shiro.UserRealm;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("defaultSecurityManager") DefaultSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        //拦截请求
        Map<String, String> filterMap = new HashMap<>();
        //所有请求都需要认证
        filterMap.put("/**", "authr");
        //登录请求不需要授权
        shiroFilterFactoryBean.setUnauthorizedUrl("/system/login");
        //登陆页面地址
        shiroFilterFactoryBean.setLoginUrl("login.html");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
        return shiroFilterFactoryBean;
    }

    @Bean("defaultSecurityManager")
    public DefaultSecurityManager getSecurityManager(@Qualifier("userRealm") UserRealm userRealm) {
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("MD5");
        matcher.setHashIterations(1024);
        userRealm.setCredentialsMatcher(matcher);
        securityManager.setRealm(userRealm);
        return securityManager;
    }

    /**
     * realm连接数据，做认证和授权
     * @return
     */
    @Bean("userRealm")
    public UserRealm getRealm() {
        return new UserRealm();
    }
}
