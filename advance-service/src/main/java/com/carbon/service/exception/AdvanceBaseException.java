package com.carbon.service.exception;

import com.carbon.dal.reponse.CodeMsg;

public class AdvanceBaseException extends RuntimeException {
    private CodeMsg codeMsg;

    public AdvanceBaseException(CodeMsg codeMsg) {
        this.codeMsg = codeMsg;
    }

    public CodeMsg getException() {
        return this.codeMsg;
    }

    public void setException(CodeMsg exception) {
        this.codeMsg = exception;
    }
}
