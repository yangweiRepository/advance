package com.carbon.service.user;

import com.carbon.dal.bean.User;

public interface IUserService {
    User queryByName(String principal);
}
