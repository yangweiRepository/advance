package com.carbon.service.user.impl;

import com.carbon.dal.bean.User;
import com.carbon.dal.mapper.user.UserMapper;
import com.carbon.dal.query.UserQuery;
import com.carbon.service.user.IUserService;
import com.carbon.utils.SlatUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    public PageInfo<User> listByQuery(UserQuery query) {
        log.debug("debug========");
        log.info("info========");
        log.error("error========");
        PageHelper.startPage(query.getPageNum(), query.getPageSize());
        List<User> users = userMapper.listByQuery(query);
        PageInfo<User> userPageInfo = new PageInfo<>(users);

        return new PageInfo<>(users);

    }

    public User listById(Integer id) {
        return userMapper.listById(id);
    }

    public List<User> getUserAll() {
        return userMapper.getUserAll();
    }

    public void insert(User user) {
        String password = user.getPassword();
        String slat = SlatUtils.generateSlat(4);
        Md5Hash md5Hash = new Md5Hash(password,slat,1024);
        user.setPassword(md5Hash.toHex());
        user.setSalt(slat);
        userMapper.insert(user);
    }

    public void insert1(Integer id,String name) {
        userMapper.insert1(id,name);
    }

    public List<User> listAll() {
        return userMapper.listAll();
    }

    public User getById(Integer id) {
        return userMapper.getById(id);
    }

    @Override
    public User queryByName(String principal) {
        return userMapper.getByName(principal);
    }
}
