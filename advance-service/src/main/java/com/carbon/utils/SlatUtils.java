package com.carbon.utils;



import java.util.Random;

public class SlatUtils {

    public static String str ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";

    public static String generateSlat(int n) {
        char[] chars = str.toCharArray();
        StringBuilder slat = new StringBuilder();
        for (int i = 0; i < n; i++) {
            char aChar = chars[new Random().nextInt(str.length())];
            slat.append(aChar);
        }
        return slat.toString();
    }

    public static void main(String[] args) {
        String s = generateSlat(4);
        System.out.println(s);
    }
}
