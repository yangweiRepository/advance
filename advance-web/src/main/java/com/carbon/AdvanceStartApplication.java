package com.carbon;

import com.carbon.dal.datasource.AdvanceWriteAndReadDataSourceConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

/**
 * @author 杨威
 * @Description
 * @Date 创建于 2021/5/19 下午8:12
 */

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
//@SpringBootApplication
@MapperScan("com.carbon.dal.mapper")
@EnableAspectJAutoProxy
//@Import({AdvanceWriteAndReadDataSourceConfig.class})
public class AdvanceStartApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdvanceStartApplication.class, args);
    }
}
