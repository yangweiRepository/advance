package com.carbon.web.auth.shiortest;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;


public class MyTestRealm extends AuthorizingRealm {
    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //授权是在授权之后  所以这里
        //模拟查询用户权限
        String primaryPrincipal = (String) principalCollection.getPrimaryPrincipal();
        //根据primaryPrincipal查询权限信息
        Set<String> singleton = Collections.singleton("admin");
        //返回用户权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo(singleton);

        simpleAuthorizationInfo.addStringPermission("user:create:*");
        return simpleAuthorizationInfo;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //1、当前主题
        String principal = (String)authenticationToken.getPrincipal();

        //2\构建token,数据库信息
        //模拟根据principal查询数据库信息

        //MD5
//        String credentials = "64c8b1e43d8ba3115ab40bcea57f010b";

        //MD5+slat
        String credentials = "221a45b41fd7dd444be7f078074273a3";

        //返回数据给securityManager  1、账号   2、加密后密码   3、salt  4、realmName
        if ("xiaozhang".equals(principal)) {
            return new SimpleAuthenticationInfo(principal,credentials, ByteSource.Util.bytes("advance".getBytes()),this.getName());
        }
        return null;
    }
}
