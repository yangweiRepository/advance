package com.carbon.web.auth.shiortest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;

public class ShiroTest {

    public static void main(String[] args) {
        //创建securityManager
        DefaultSecurityManager securityManager = new DefaultSecurityManager();

        //设置自定义的realm   realm是连接数据库的获取认证数据，并进行账户和密码的匹配
        MyTestRealm myTestRealm = new MyTestRealm();

        //realm是连接数据库的获取认证数据，并进行账户和密码的匹配,原生的matcher是equals比较，如果我们进行加密
        //就需要给realm指定对应的matcher
        //hash散列
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        //加密方式
        hashedCredentialsMatcher.setHashAlgorithmName("MD5");
        //散列次数
        hashedCredentialsMatcher.setHashIterations(1024);

        //给realm设置matcher
        myTestRealm.setCredentialsMatcher(hashedCredentialsMatcher);
        //给securityManager设置realm
        securityManager.setRealm(myTestRealm);

        //将自定义的securityManager交给shiro
        SecurityUtils.setSecurityManager(securityManager);

        //模拟用户登陆数据，创建token
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("xiaozhang","123");

        //获取用户主体
        Subject subject = SecurityUtils.getSubject();

        //登录
        System.out.println("isAuthenticated:" + subject.isAuthenticated());
        try {
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException e) {
            System.out.println("========用户名错误");
            e.printStackTrace();
        } catch (IncorrectCredentialsException e) {
            System.out.println("========密码错误");
            e.printStackTrace();
        } catch (AuthenticationException e) {
            System.out.println("========登陆失败");
        }

        System.out.println("isAuthenticated:" + subject.isAuthenticated());


        //授权--认证之后
        if (subject.isAuthenticated()) {
            //debug：这里的顺序是先认真，认证完成后设计到权限的代码的时候，才会去走授权的代码

            //基于角色
            System.out.println("===================="+subject.hasRole("admin"));

            //基于资源（权限）
            System.out.println("subject.isPermitted(\"user:create:*\") = " + subject.isPermitted("user:create:*"));
            System.out.println("subject.isPermitted(\"user:create:001\") = " + subject.isPermitted("user:create:001"));
        }
    }
}
