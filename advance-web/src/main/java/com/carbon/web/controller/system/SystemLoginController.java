package com.carbon.web.controller.system;


import com.carbon.dal.bean.User;
import com.carbon.dal.reponse.CommonResponse;
import com.carbon.service.user.impl.UserServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class SystemLoginController {

    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/login")
    public String login(@RequestBody User user) {
        Subject subject = SecurityUtils.getSubject();
        AuthenticationToken token=new UsernamePasswordToken(user.getName(),user.getPassword());
        subject.login(token);
        return "redirect: index";
    }

    @PostMapping("/register")
    public String register(@RequestBody User user) {
        userService.insert(user);
        return "redirect: login";
    }

}
