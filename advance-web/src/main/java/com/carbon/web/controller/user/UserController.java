package com.carbon.web.controller.user;

import com.carbon.dal.bean.User;
import com.carbon.dal.datasource.CurrentDataSource;
import com.carbon.dal.datasource.ReadModel;
import com.carbon.dal.query.UserQuery;
import com.carbon.dal.reponse.CodeMsg;
import com.carbon.dal.reponse.CommonResponse;
import com.carbon.service.exception.AdvanceBaseException;
import com.carbon.service.user.impl.UserServiceImpl;
import com.carbon.utils.SlatUtils;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping("/list/page")
    @CurrentDataSource(name = ReadModel.MASTER)
    public CommonResponse listByQuery(@RequestBody UserQuery query) {
        //参数处理
        PageInfo<User> userPageInfo = userService.listByQuery(query);
        return CommonResponse.success(userPageInfo);
    }

    @GetMapping("/list/master")
    @CurrentDataSource(name = ReadModel.MASTER)
    public CommonResponse listAllMaster() {
        //参数处理
        List<User> userPageInfo = userService.listAll();
        return CommonResponse.success(userPageInfo);
    }

    @GetMapping("/list/slave")
    @CurrentDataSource(name = ReadModel.SLAVE)
    public CommonResponse listAllSlave() {
        //参数处理
        List<User> userPageInfo = userService.listAll();
        return CommonResponse.success(userPageInfo);
    }

    @PostMapping("/insert")
    @CurrentDataSource(name = ReadModel.MASTER)
    public CommonResponse insert(@RequestBody User user) {
        userService.insert(user);
        return CommonResponse.success();
    }

}
