package com.carbon.web.exception;
import com.carbon.dal.reponse.CodeMsg;
import com.carbon.dal.reponse.CommonResponse;
import com.carbon.service.exception.AdvanceBaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局处理异常 自己抛出的  和 系统异常
 */
//@RestControllerAdvice
@Slf4j
public class GlobalDefaultException {


    @ExceptionHandler(AdvanceBaseException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public CommonResponse sendErrorResponse_UserDefined(Exception exception){
        log.error("errorMessage==>:{}",exception.getMessage());
        exception.printStackTrace();
        return CommonResponse.error(((AdvanceBaseException)exception).getException());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public CommonResponse sendErrorResponse_System(Exception exception){
        if (exception instanceof AdvanceBaseException) {
            return CommonResponse.error(((AdvanceBaseException)exception).getException());
        }
        //Exception exception可以获取详细信息
        log.error("errorMessage==>:{}",exception.getMessage());
        exception.printStackTrace();
        return  CommonResponse.error(new CodeMsg(HttpStatus.INTERNAL_SERVER_ERROR.value(),exception.getMessage()));
    }
}
